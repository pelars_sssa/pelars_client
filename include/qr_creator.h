#pragma once
#include "opt.h"

extern bool to_stop;

cv::Mat drawQr(int width, int repetitions, int session);
void showQr(int session);